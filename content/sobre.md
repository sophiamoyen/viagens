---
title: "Sobre"
date: 2021-10-22T16:43:13-03:00
child1: /images/about-1.JPG
child2: /images/about-2.jpeg
child3: /images/about-3.JPG
draft: false
---

A autora deste blog sou eu, Sophia Moyen, muito interessada por conhecer culturas, línguas e paisagens diferentes. Apesar de não ter visitado parte dos países sobre os quais publico, espero um dia conhecê-los. Quando se sabe a história sobre um lugar antes de visitá-lo, a experiência é completamente outra. Por isso, criei este blog. Espero que aqueles que têm condições de viajar e visitar os mais diversos países saibam para onde estão indo e a história por trás de seu povo.

Ao mesmo tempo que aprendo publicando sobre diferentes lugares, também aprendo a programar um site ;)