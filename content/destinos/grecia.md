---
title: "Grécia"
date: 2021-10-22T16:42:57-03:00
child1: /images/grecia-1.png
child2: /images/grecia-2.jfif
child3: /images/grecia-3.jfif
draft: false
---

Grécia (em grego: Ελλάδα), oficialmente República Helênica (português brasileiro) e historicamente conhecida como Hélade (em grego: Ἑλλάς), é um país localizado no sul da Europa.[5] De acordo com dados do censo de 2011, a população grega é de cerca de 11 milhões de pessoas. Atenas é a capital e a maior cidade do país.

O país está estrategicamente localizado no cruzamento entre a Europa, a Ásia, o Oriente Médio e a África.[6][7][8] Tem fronteiras terrestres com a Albânia a noroeste, com a Macedônia do Norte e a Bulgária ao norte e com a Turquia no nordeste. O país é composto por nove regiões geográficas: Macedônia, Grécia Central, Peloponeso, Tessália, Epiro, Ilhas Egeias (incluindo o Dodecaneso e Cíclades), Trácia, Creta e Ilhas Jônicas. O Mar Egeu fica a leste do continente, o Mar Jônico a oeste e o Mar Mediterrâneo ao sul. A Grécia tem a 11.ª maior costa do mundo, com 13 676 quilômetros de comprimento, com um grande número de ilhas (cerca de 1 400, das quais 227 são habitadas). Oitenta por cento do país é composto por montanhas, das quais o Monte Olimpo é a mais elevada, a 2 917 metros de altitude.

A Grécia moderna tem suas raízes na civilização da Grécia Antiga, considerada o berço de toda a civilização ocidental. Como tal, é o local de origem da democracia, da filosofia ocidental,[9] dos Jogos Olímpicos, da literatura ocidental, da historiografia, da ciência política, de grandes princípios científicos e matemáticos,[10] das artes cênicas ocidentais,[11] incluindo a tragédia e a comédia. As conquistas culturais e tecnológicas gregas influenciaram grandemente o mundo, sendo que muitos aspectos da civilização grega foram transmitidos para o Oriente através de campanhas de Alexandre, o Grande, e para o Ocidente, através do Império Romano. Este rico legado é parcialmente refletido nos 17 locais considerados pela UNESCO como Patrimônio Mundial no território grego, o sétimo maior número da Europa e o 13.º do mundo. O Estado grego moderno, que engloba a maior parte do núcleo histórico da civilização grega antiga, foi criado em 1830, após a Guerra da Independência Grega contra o antigo Império Otomano.

Atualmente, a Grécia é um país democrático[12] e desenvolvido,[13][14] com uma economia avançada e de alta renda,[15] um alto padrão de vida[16][17] e um índice de desenvolvimento humano (IDH) considerado muito alto pelas Nações Unidas.[4] A Grécia é um membro fundador da Organização das Nações Unidas (ONU), é membro do que é hoje a União Europeia desde 1981 (e da Zona Euro desde 2001),[18] além de ser membro da Organização do Tratado do Atlântico Norte (OTAN) desde 1952. A economia grega é também a maior dos Balcãs, onde a Grécia é um importante investidor regional.