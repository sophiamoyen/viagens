---
title: "Suécia"
date: 2021-10-22T16:42:57-03:00
child1: /images/suecia-1.png
child2: /images/suecia-2.jpg
child3: /images/suecia-3.jpg
draft: false
---

A Suécia (em sueco: Sverige), oficialmente Reino da Suécia, é um país nórdico, localizado na península Escandinava na Europa do Norte. Tem fronteiras terrestres com a Noruega, a oeste, e com a Finlândia, a nordeste, sendo banhada pelo Mar Báltico a leste e a sul. Está separada da Dinamarca a sudoeste pelo estreito de Öresund, sobre o qual corre a ponte de Öresund.[5][6][7][8][9]

Com uma área terrestre de 407 311 km², um comprimento de 1 572 km e uma largura de 499 km, a Suécia é o terceiro maior país da União Europeia em termos de superfície. É constituída por um terreno plano ou ondulado na sua parte sul, enquanto a parte norte apresenta uma planície costeira seguida de um interior acidentado culminando em alta montanha junto à fronteira com a Noruega.[10]

Possui uma população total superior a 10 milhões de habitantes (2018). Apresenta uma baixa densidade populacional, com cerca de 23 habitantes por quilômetro quadrado, mas conta todavia com uma densidade consideravelmente maior na metade sul do país. Cerca de 85% da população vive em áreas urbanas. A capital e maior cidade do país é Estocolmo (com uma população de 1,3 milhão na área urbana e de 2 milhões na área metropolitana), centro do poder político e econômico do país.[1][11][12][13]

É uma monarquia constitucional com um sistema parlamentar de governo e um monarca com funções unicamente representativas.[10] Tem uma economia altamente desenvolvida e diversificada, largamente baseada hoje em dia em serviços.[10] O país ocupa o quarto lugar do mundo no Índice de democracia, depois da Islândia, da Dinamarca e da Noruega, segundo a prestigiada revista inglesa The Economist. O país ainda é considerado um dos mais socialmente justos da atualidade, apresentando um dos mais baixos níveis de desigualdade de renda do mundo. A Suécia é membro fundador da Organização das Nações Unidas, da União Europeia desde 1 de janeiro de 1995, e da OCDE.[14] Isso se reflete no fato da Suécia estar, desde que a ONU começou a calcular o IDHAD (Índice de Desenvolvimento Humano Ajustado à Desigualdade) de seus membros, entre os mais bem colocados países do mundo de acordo com esse indicador.[15]

A Suécia emergiu como um país independente e unificado durante a Idade Média. No século XVII o país expandiu seus territórios para formar o Império Sueco. A maior parte dos territórios conquistados fora da península Escandinava foram perdidos durante os séculos XVIII e XIX. A metade oriental da Suécia, o que hoje é a Finlândia, foi perdida para a Rússia em 1809. A última guerra na qual a Suécia esteve diretamente envolvida foi em 1814, quando a Suécia forçou por meios militares a Noruega a se juntar ao país e criar o Reinos Unidos da Suécia e Noruega, uma união que durou até 1905. Desde então, a Suécia ficou em paz, com a adoção de uma política externa não alinhada em tempos de paz e de neutralidade em tempo de guerra.[16]